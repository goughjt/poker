# -*- coding: utf-8 -*-
"""
poker test
"""

import pytest
from poker.hand import Hand

@pytest.fixture
def hands():
    """
    Return an object, with properties given as: "{rank}_{relative_order}"
    Rank is, e.g. "flush"
    Relative order gives higher values to "better" hands
    """
    hands = type("hands", (), {})
    hands.high_card_1 =  Hand((2, 'H'), (3, 'S'), (9, 'D'), (12, 'H'), (13, 'C'))
    hands.high_card_1b =  Hand((2, 'S'), (3, 'C'), (9, 'H'), (12, 'C'), (13, 'H'))
    hands.high_card_2 =  Hand((2, 'C'), (3, 'H'), (9, 'S'), (12, 'D'), (14, 'H'))
    hands.high_card_3 =  Hand((2, 'C'), (3, 'H'), (9, 'S'), (13, 'D'), (14, 'H'))
    hands.pair_1 =  Hand((2, 'H'), (2, 'S'), (9, 'D'), (12, 'H'), (13, 'C'))
    hands.pair_2 =  Hand((3, 'H'), (3, 'S'), (9, 'H'), (12, 'S'), (13, 'H'))
    hands.two_pair_1 =  Hand((2, 'H'), (2, 'S'), (4, 'D'), (4, 'H'), (13, 'C'))
    hands.two_pair_2 =  Hand((3, 'H'), (3, 'S'), (8, 'H'), (8, 'S'), (13, 'H'))
    hands.three_of_kind_1 =  Hand((2, 'H'), (2, 'S'), (2, 'D'), (4, 'H'), (13, 'C'))
    hands.three_of_kind_2 =  Hand((3, 'H'), (3, 'S'), (3, 'H'), (8, 'S'), (13, 'H'))
    hands.straight_1 =  Hand((2, 'H'), (3, 'S'), (4, 'D'), (5, 'H'), (6, 'C'))
    hands.straight_2 =  Hand((3, 'H'), (4, 'S'), (5, 'D'), (6, 'H'), (7, 'C'))
    hands.flush_1 =  Hand((2, 'H'), (6, 'H'), (3, 'H'), (4, 'H'), (13, 'H'))
    hands.flush_2 =  Hand((2, 'C'), (6, 'C'), (3, 'C'), (5, 'C'), (13, 'C'))
    hands.full_house_1 =  Hand((2, 'H'), (2, 'S'), (3, 'D'), (3, 'H'), (3, 'C'))
    hands.full_house_2 =  Hand((2, 'H'), (2, 'S'), (4, 'D'), (4, 'H'), (4, 'C'))
    hands.four_of_kind_1 =  Hand((2, 'H'), (2, 'S'), (2, 'D'), (4, 'H'), (2, 'C'))
    hands.four_of_kind_2 =  Hand((3, 'H'), (3, 'S'), (3, 'D'), (4, 'C'), (3, 'C'))
    hands.straight_flush_1 =  Hand((2, 'H'), (3, 'H'), (4, 'H'), (5, 'H'), (6, 'H'))
    hands.straight_flush_2 =  Hand((2, 'C'), (3, 'C'), (4, 'C'), (5, 'C'), (6, 'C'))
    return hands

def test_hand_needs_exactly_five_cards():
    assert Hand((2, 'C'), (3, 'H'), (9, 'S'), (12, 'D'), (14, 'H'))

def test_hand_cannot_have_less_than_five_cards():
    with pytest.raises(TypeError):
        Hand((3, 'H'), (9, 'S'), (12, 'D'), (14, 'H'))

def test_hand_cannot_have_more_than_five_cards():
    with pytest.raises(TypeError):
        Hand((2, 'C'), (3, 'H'), (9, 'S'), (12, 'D'), (14, 'H'), (3, 'C'))

def test_hands_high_cards_are_comparable(hands):
    assert hands.high_card_1 < hands.high_card_2

def test_hands_second_hand_higher_card(hands):
    assert hands.high_card_2 < hands.high_card_3

def test_equal_hands_result_in_draw(hands):
    assert hands.high_card_1 == hands.high_card_1b

def test_high_card_rank(hands):
    assert hands.high_card_1.rank_type == Hand.RankType.HIGH_CARD

def test_pair_rank(hands):
    assert hands.pair_1.rank_type == Hand.RankType.PAIR

def test_two_pair_rank(hands):
    assert hands.two_pair_1.rank_type == Hand.RankType.TWO_PAIR

def test_three_of_kind_rank(hands):
    assert hands.three_of_kind_1.rank_type == Hand.RankType.THREE_OF_KIND

def test_straight_rank(hands):
    assert hands.straight_1.rank_type == Hand.RankType.STRAIGHT

def test_flush_rank(hands):
    assert hands.flush_1.rank_type == Hand.RankType.FLUSH

def test_full_house_rank(hands):
    assert hands.full_house_1.rank_type == Hand.RankType.FULL_HOUSE

def test_four_of_kind_rank(hands):
    assert hands.four_of_kind_1.rank_type == Hand.RankType.FOUR_OF_KIND

def test_straight_flush_rank(hands):
    assert hands.straight_flush_1.rank_type == Hand.RankType.STRAIGHT_FLUSH

def test_pair_hands_are_comparable(hands):
    assert hands.pair_1 < hands.pair_2

def test_two_pair_hands_are_comparable(hands):
    assert hands.two_pair_1 < hands.two_pair_2

def test_three_of_kind_hands_are_comparable(hands):
    assert hands.three_of_kind_1 < hands.three_of_kind_2

def test_straight_hands_are_comparable(hands):
    assert hands.straight_1 < hands.straight_2

def test_flush_hands_are_comparable(hands):
    assert hands.flush_1 < hands.flush_2

def test_full_house_hands_are_comparable(hands):
    assert hands.full_house_1 < hands.full_house_2

def test_four_of_kind_hands_are_comparable(hands):
    assert hands.four_of_kind_1 < hands.four_of_kind_2

def test_straight_flush_hands_are_equal(hands):
    assert hands.straight_flush_1 == hands.straight_flush_2

def test_pair_beats_high_card(hands):
    assert hands.high_card_1 < hands.pair_1

def test_two_pair_beats_pair(hands):
    assert hands.pair_1 < hands.two_pair_1

def test_three_of_kind_beats_two_pair(hands):
    assert hands.two_pair_1 < hands.three_of_kind_1

def test_straight_beats_three_of_kind(hands):
    assert hands.three_of_kind_1 < hands.straight_1

def test_flush_beats_straight(hands):
    assert hands.straight_1 < hands.flush_1

def test_full_house_beats_flush(hands):
    assert hands.flush_1 < hands.full_house_1

def test_four_of_kind_beats_full_house(hands):
    assert hands.full_house_1 < hands.four_of_kind_1

def test_straight_flush_beats_four_of_kind(hands):
    assert hands.four_of_kind_1 < hands.straight_flush_1
