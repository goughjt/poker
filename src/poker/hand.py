from enum import IntEnum

class Hand():
    """
    A classic 5-card in poker, represented internally as a list of 5 tuples (value, suit).
    For values,, 2 = 2, 3 = 3, ..., 9 = 9, 10 = 10, 11 = jack, 12 = queen, 13 = king, 14 = ace
    """

    class RankType(IntEnum):
        HIGH_CARD = 1
        PAIR = 2
        TWO_PAIR = 3
        THREE_OF_KIND = 4
        STRAIGHT = 5
        FLUSH = 6
        FULL_HOUSE = 7
        FOUR_OF_KIND = 8
        STRAIGHT_FLUSH = 9

    def __init__(self, card1, card2, card3, card4, card5):
        self.cards = [card1, card2, card3, card4, card5]
        self.values= sorted([i for (i, j) in self.cards])
        self.suits= [j for (i, j) in self.cards]
        (self.rank_type, self.significant_values) = self.rank()

    def __gt__(self, other_hand):
        """
        Checks is self is greater than other_hand
        """
        if self.rank_type != other_hand.rank_type:
            return self.rank_type > other_hand.rank_type
        for i in range(len(self.significant_values)):
            if self.significant_values[i] != other_hand.significant_values[i]:
                return self.significant_values[i] > other_hand.significant_values[i]
        return False

    def __eq__(self, other_hand):
        """
        Checks is self is equal to other_hand
        """
        if self.rank_type != other_hand.rank_type:
            return False;
        return self.significant_values == other_hand.significant_values

    def rank(self):
        unique_suits_count = len(set(self.suits))
        unique_values_count = len(set(self.values))
        max_repeated_value_count = max([self.values.count(i) for i in self.values])
        single_values = sorted(list(set([i for i in self.values if self.values.count(i) == 1])), reverse=True)
        double_values = sorted(list(set([i for i in self.values if self.values.count(i) == 2])), reverse=True)
        triple_values = sorted(list(set([i for i in self.values if self.values.count(i) == 3])), reverse=True)
        quadruple_values = sorted(list(set([i for i in self.values if self.values.count(i) == 4])), reverse=True)
        significant_values = quadruple_values + triple_values + double_values + single_values
        if unique_suits_count == 1 and [i - self.values[0] for i in self.values] == [0, 1, 2, 3, 4]:
            rank_type = self.RankType.STRAIGHT_FLUSH
        elif max_repeated_value_count == 4:
            rank_type = self.RankType.FOUR_OF_KIND
        elif unique_values_count == 2 and max_repeated_value_count == 3:
            rank_type = self.RankType.FULL_HOUSE
        elif unique_suits_count == 1:
            rank_type = self.RankType.FLUSH
        elif [i - self.values[0] for i in self.values] == [0, 1, 2, 3, 4]:
            rank_type = self.RankType.STRAIGHT
        elif unique_values_count == 3 and max_repeated_value_count == 3:
            rank_type = self.RankType.THREE_OF_KIND
        elif unique_values_count == 3 and max_repeated_value_count == 2:
            rank_type = self.RankType.TWO_PAIR
        elif unique_values_count == 4:
            rank_type = self.RankType.PAIR
        else:
            rank_type = self.RankType.HIGH_CARD
        return (rank_type, significant_values)
