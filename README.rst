=====
poker
=====


A simple little project for comparing poker hands.


Description
===========

Not much else to say, we can check if hands are greater than or equal.

Run the tests with:

```
python setup.py test
```


Note
====

This project has been set up using PyScaffold 3.0.2. For details and usage
information on PyScaffold see http://pyscaffold.org/.
